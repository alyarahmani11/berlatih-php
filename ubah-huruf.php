<?php
function ubah_huruf($string){
//kode di sini
	$alfabet = "abcdefghijklmnopqrstuvwxyz";
	$hasil = "";
	$ubah = "";
	$length = strlen($string);
	for ($i=0; $i < $length; $i++) { 
		$posisi = strpos($alfabet, $string[$i]);
		$hasil = $alfabet[$posisi+1];
		$ubah .= $hasil;
	}
	return $ubah . "<br>";	
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>