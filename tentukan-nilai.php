<?php
function tentukan_nilai($number)
{
    //  kode disini    

    if (85 <= $number && $number <= 100) {
    	return "Sangat Baik <br>";
    }
    else if (70 <= $number && $number < 85) {
    	return "Baik <br>";
    }
    else if (60 <= $number && $number < 70) {
    	return "Cukup <br>";
    }
    else {
    	return "Kurang <br>";
    }
}


//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>